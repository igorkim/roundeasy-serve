# README #

### What is this repository for? ###

* Simple serving API built with Node.JS Express Framework
* [View live](https://kim.roundeasy.ru)

### Requirements ###

* NodeJS

### Installation ###
Project built with npm, so in project folder just run:
```
#!bash
npm install
```

### Usage ###
To start API server:
```
#!bash
npm start
```