var express = require('express');
var config = require('../config');
var router = express.Router();

router.get('/', function (req, res, next) {
    config.mongodb.collection("certificates").find({}, null, {sort: {sort: 1}}).toArray(function (err, docs) {
        docs.forEach(function(element, index, array){
            docs[index].imageUrl = config.BASE_URL + element.imageUrl;
        });
        res.send(docs);
    });
});

module.exports = router;
