var express = require('express');
var config = require('../config');
var router = express.Router();

router.get('/', function(req, res, next) {
    config.mongodb.collection("articles").find({}, null, { sort: { code: 1 } }).toArray(function(err, docs) {
        res.send(docs);
    });
});
router.get('/:code', function(req, res, next) {
    console.log(req.params.code);
    config.mongodb.collection("articles").find({ code: req.params.code }, null, { sort: { code: 1 } }).toArray(function(err, docs) {
        docs.forEach(function(element, index, array) {
            docs[index].imageUrl = config.BASE_URL + element.imageUrl;
        });
        res.send(docs);
    });
});

module.exports = router;
