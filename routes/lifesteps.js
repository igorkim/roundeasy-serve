var express = require('express');
var config = require('../config');
var router = express.Router();

/* GET users listing. */
router.get('/', function (req, res, next) {
    config.mongodb.collection("lifesteps").find().toArray(function (err, docs) {
        res.send(docs);
    });
});

module.exports = router;
